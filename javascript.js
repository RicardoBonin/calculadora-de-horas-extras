		const btnCalcular = document.getElementById("calcular-resultado")

		

		const calcularHoraSalario = (salario, cargaHorariaSelecionada) => parseInt(salario) / parseInt(cargaHorariaSelecionada);
		const calcularValorHora50 = (horaSalario) => horaSalario * 0.5;
		const calcularValorTotal50 = (valorHora50, horaSalario, hora50) => (valorHora50 + horaSalario) * hora50;
		const calcularValorHora100 = (horaSalario) => horaSalario * 2;
		const calcularValorTotal100 = (hora100, valorHora100) => valorHora100 * hora100;
		const calcularValorHoraNot = (horaSalario) => horaSalario * 0.2;
		const calcularValorTotalNot = (valorHoraNot,horaNoturna) => valorHoraNot * horaNoturna;
		const calcularValorHoraNot120 = (horaSalario) => horaSalario * 3 * 0.2;
		const calcularValorTotalNot120 = (valorHoraNot120, horaNoturna120) => valorHoraNot120 * horaNoturna120;
		const gerarMensagem = (valorTotal50, valorTotal100, valorTotalNot, valorTotalNot120) => 			 
			 `
			Valor Total de horas 50%: ${valorTotal50.toFixed(2)}
			Valor Total de horas 100%: ${valorTotal100.toFixed(2)}
			Valor Total adicional noturno: ${valorTotalNot.toFixed(2)}
			Valor Total de horas 120%: ${valorTotalNot120.toFixed(2)}
			`;

		function imprimirResultado () {
			const hora50 = document.getElementById("hora-50").value;
			const hora100 = document.getElementById("hora-100").value;
			const horaNoturna = document.getElementById("hora-noturna").value;
			const horaNoturna120 = document.getElementById("hora-noturna-120").value;		
			const btnCalcular = document.getElementById("calcular-resultado");
			const radio = document.getElementsByName('cargaHoraria');
			const salario = document.getElementById("salario").value;
			const cargaHorariaSelecionada = [...radio].find(function(item) {
	   	 		return item.checked === true
			});  
			
			const horaSalario = calcularHoraSalario(salario, cargaHorariaSelecionada.value);
			const valorHora50 = calcularValorHora50(horaSalario);
			const valorTotal50 = calcularValorTotal50(valorHora50, horaSalario, hora50);
			const valorHora100 = calcularValorHora100(horaSalario);
			const valorTotal100 = calcularValorTotal100(hora100, valorHora100);
			const valorHoraNot = calcularValorHoraNot(horaSalario);
			const valorTotalNot = calcularValorTotalNot(valorHoraNot, horaNoturna);
			const valorHoraNot120 = calcularValorHoraNot120(horaSalario);
			const valorTotalNot120 = calcularValorTotalNot120(valorHoraNot120, horaNoturna120);

			document.getElementById('resultado').innerHTML = gerarMensagem(valorTotal50, valorTotal100, valorTotalNot, valorHoraNot120);
			
		}	
		
		btnCalcular.addEventListener("click",imprimirResultado);
		